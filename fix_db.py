import sys

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import pandas as pd

from selenium import webdriver

from books_scanner.commons import (
    searchISBN,
    saveDF
)

'''
Go through CSV file and look up online the missing book titles.
'''

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Usage: python {} db.csv')
        exit(0)

    filename = str(sys.argv[1])

    df = pd.read_csv(filename)
    new_df = pd.DataFrame(columns=df.columns)
    refs = {}
    shops = ['gibert', 'momox', 'kiwibook']

    driver = webdriver.Firefox()

    for idx, row in df.iterrows():
        for shop in shops:
            if row[shop] == 0.0: row[shop] = None

        if pd.isna(row['title']):
            if row['isbn'] in refs.keys():
                book_title = refs[row['isbn']]
            else:
                print('Searching title for ISBN {} ...'.format(row['isbn']))
                book_title = searchISBN(driver, row['isbn'])

                if book_title != '':
                    print('Found title: {}'.format(book_title))
                    refs.update({row['isbn']: book_title})

            if book_title != '':
                row['title'] = book_title
        else:
            refs.update({row['isbn']: row['title']})

        if not pd.isna(row['title']):
            new_df = new_df.append(row)

    saveDF(new_df, filename)
    driver.close()
