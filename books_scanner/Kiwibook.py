from .commons import (
    waitForPrice
)

from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

def searchKiwibook(driver, v, verbose=False):
    price = None
    url = 'https://vendre-livre.fr/?ean13={}&ref=kiwibook_form'.format(v)
    submit_button = 'submit'

    if verbose: print('[INFO] Requesting Kiwibook url {}...'.format(url))
    driver.get(url)
    driver.find_element_by_id(submit_button).click()

    return waitForPrice(driver, By.TAG_NAME, 'price')

def searchBookToBuyOnKiwibook(driver, isbn, add_to_cart=False):
    price = None
    search_form_id = 'formulaire_form'
    search_input_name = 'searreq'

    search_field_xpath = ''.join(["//form[@id='",
                                  search_form_id,
                                  "']",
                                  "/div[1]",
                                  "/div[2]",
                                  "/input[@name='",
                                  search_input_name,
                                  "']"])

    search_field = driver.find_element_by_xpath(search_field_xpath)
    search_field.clear()
    search_field.send_keys(str(isbn))
    search_field.send_keys(Keys.RETURN)

    WebDriverWait(driver, 1).until(
        EC.element_to_be_clickable((By.XPATH,
                                    "//div[@class='l_product_item']/div[2]/a"))
    ).click()

    try:
        price = waitForPrice(driver, By.CLASS_NAME, "pprice")

        if add_to_cart:
            WebDriverWait(driver, 1).until(
                EC.element_to_be_clickable((By.CLASS_NAME, 'productCart'))
            ).click()
            WebDriverWait(driver, 1).until(
                EC.element_to_be_clickable((By.ID, 'continue-shopping'))
            ).click()
    except (TimeoutException, NoSuchElementException):
        pass

    return price

kiwibook_params = {'url_buy'         : 'https://www.kiwibook.fr/',
                   'seller'          : 'kiwibook',
                   'cookie_button_id': None,
                   'search_field_id' : 'formulaire_form',
                   'method'          : By.ID,
                   'search_func'     : searchBookToBuyOnKiwibook,
                   'add_to_cart'     : True}
