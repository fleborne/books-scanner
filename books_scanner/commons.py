import time

from selenium import webdriver

from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

def convertPrice(price_str):
    ''' Parse a string price (e.g. 9,67 €) as a float (9.67). '''
    return float(price_str.replace('€', '') \
                          .replace(',', '.') \
                          .strip())

def checkISBN13(v):
    ''' Given an EAN13 barcode, check that the check digit is valid

    :param v: the barcode value as a string
    :return b: True if check digit is valid, False otherwise
    '''
    assert type(v) is str
    if len(v) != 13: return False

    v_str = v[0:-1][::-1]
    odd_sum = sum([int(c) for c in v_str[0::2]])
    even_sum = sum([int(c) for c in v_str[1::2]])
    sum_ = 3 * odd_sum + even_sum

    if sum_ % 10 == 0:
        check_digit = 0
    else:
        check_digit = 10 * (1 + (sum_ // 10)) - sum_

    return int(v[-1]) == check_digit

def checkISBN10(v):
    ''' Given an ISBN 10 barcode, check that the check digit is valid

    :param v: the barcode value as a string
    :return b: True if check digit is valid, False otherwise
    '''
    assert type(v) is str
    if len(v) != 10: return False

    sum_ = 0

    for i, c in enumerate(v[0:-1]):
        sum_ += int(c) * (i+1)

    return int(v[-1]) == (sum_ % 11)

def checkBarCode(v):
    assert type(v) is str
    if len(v) == 10:
        return checkISBN10(v)
    elif len(v) == 13:
        return checkISBN13(v)
    else:
        raise ValueError('ISBN must have length 10 or 13')

def saveDF(df, csv_file, verbose=False):
    ''' Save a dataframe to CSV file and esures that the data types are correct.

    :param df: The dataframe to save
    :param csv_file: The path to the destination CSV file
    :param verbose: (optional, default=False) Set to True to print debug info.
    '''

    if verbose: print('[INFO] Saving dataframe to {}'.format(csv_file))
    df['isbn'] = df['isbn'].astype(str)
    df.to_csv(csv_file, index=False)

def searchISBNSearch(driver, v):
    ''' Search a book's ISBN 13 to retrieve the book's title on isbnsearch.org

    :param v: The ISBN 13.
    :return: The book title if found, an empty string otherwise.
    '''

    url = 'https://isbnsearch.org/isbn/{}'.format(v)
    driver.get(url)

    try:
        book_title = driver.find_element_by_xpath("//div[@class='bookinfo']/h1").text
    except:
        book_title = ''

    return book_title

def searchISBNdb(driver, v):
    ''' Search a book's ISBN 13 to retrieve the book's title on isbndb.com

    :param v: The ISBN 13.
    :return: The book title if found, an empty string otherwise.
    '''
    url = "https://isbndb.com/book/{}".format(v)
    driver.get(url)

    try:
        book_title = driver.find_element_by_xpath(
            "//div[@id='block-multipurpose-business-theme-page-title']/h1").text

        if book_title == 'Book not found':
            book_title = ''
    except:
        book_title = ''

    return book_title

def getTitle(driver, v):
    url = 'https://www.momox.fr/offer/{}'.format(v)
    driver.get(url)

    try:
        title_field = WebDriverWait(driver, 2).until(
            EC.presence_of_element_located((By.XPATH, "//div[@class='product-title']/h1[1]"))
        )

        return title_field.text
    except TimeoutException:
        return ''

def searchISBN(driver, v):
    ''' Search a book's ISBN 13 to retrieve the book's title.

    :param v: The ISBN 13.
    :return: The book title if found, an empty string otherwise.
    '''
    book_title = searchISBNdb(driver, v)
    if book_title != '': return book_title

    book_title = searchISBNSearch(driver, v)
    if book_title != '': return book_title

    book_title = getTitle(driver, v)
    if book_title != '': return book_title

    return ''

def waitForPrice(driver, id_method, id_value, verbose=False):
    ''' Wait for the price to be displayed (useful when the price is loaded
        after the rest of the page).

    :return: The price if found, None otherwise (type float)
    '''

    count = 0
    price = None

    try:
        price_field = WebDriverWait(driver, 1).until(
            EC.presence_of_element_located((id_method, id_value)))

        if verbose: print('[DEBUG] price_field.text', price_field.text)

        while count < 2 and price_field.text == '':
            count += 1
            time.sleep(0.5)

        if count < 5:
            try:
                price = convertPrice(price_field.text)
            except ValueError:
                print('[ERROR] Could not convert string to float: {}'.format(price_field.text))
        else:
            if verbose: print('[WARN] Price field not found')
    except TimeoutException:
        if verbose: print('[WARN] TimeoutException in waitForPrice')

    return price
