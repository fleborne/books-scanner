from .commons import (
    waitForPrice,
)

# web driver setup
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def searchGibert(driver, v, verbose=False):
  price = None
  url = 'https://www.gibert.com/sao'
  if verbose: print('[INFO] Requesting Gibert url {}...'.format(url))

  submit_button = 'add_sao'
  delete_button_class = 'action-delete'

  driver.get(url)
  driver.find_element_by_xpath("//input[@name='codes']").send_keys(v)
  driver.find_element_by_id(submit_button).click()

  xpath = "//tbody[@id='product_list']/tr[1]/td[2]/span[@class='price']"
  price = waitForPrice(driver, By.XPATH, xpath)

  # delete book from shopping cart
  try:
      delete_button = WebDriverWait(driver, 1).until(
          EC.presence_of_element_located((By.CLASS_NAME, delete_button_class)))
      driver.get(delete_button.get_attribute("href"))
  except TimeoutException:
      pass

  return price

gibert_params = {'url_buy': 'https://www.gibert.com/',
                 'seller' : 'gibert'}
