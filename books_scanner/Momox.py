import time

from .commons import (
    waitForPrice
)

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

def searchMomox(driver, v, verbose=False):
    price = None
    url = 'https://www.momox.fr/offer/{}'.format(v)
    price_class = 'searchresult-price'

    if verbose: print('[INFO] Requesting Momox url {}...'.format(url))
    driver.get(url)

    return waitForPrice(driver, By.CLASS_NAME, price_class)

def searchBookToBuyOnMomox(driver, isbn, add_to_cart=False):
    price = None
    search_field_id = 'f_search_param'
    price_field_id = 'mx-details-price'

    search_field = driver.find_element_by_id(search_field_id)
    search_field.clear()
    search_field.send_keys(str(isbn))
    search_field.send_keys(Keys.RETURN)
    time.sleep(1)

    price = waitForPrice(driver, By.ID, price_field_id)

    if add_to_cart:
        driver.find_element_by_xpath("//button[@value='IN DEN WARENKORB']").click()

    return price

momox_params = {'url_buy'         : 'https://www.momox-shop.fr/',
                'seller'          : 'momox',
                'cookie_button_id': 'cookieconsent-settings-popup-accept-all-button',
                'search_field_id' : 'f_search_param',
                'method'          : By.ID,
                'search_func'     : searchBookToBuyOnMomox,
                'add_to_cart'     : True}
