import os
import sys
import cv2
from barcode import EAN13
from barcode.writer import ImageWriter

def editAndSaveBarcode(barcode_value, filename):
    with open(filename, 'wb') as f:
        EAN13(str(barcode_value), writer=ImageWriter()).write(f)

def displaySavedImage(filename, win_name='barcode'):
    cv2.imshow(win_name, cv2.imread(filename))

def formatBarode(raw_value, length=13):
    return raw_value.strip().zfill(length)

def displayListOfBarcodes(input_filename, tmp_filename='barcode.jpeg'):
    with open(input_filename, 'r') as input_file:
        for line in input_file:
            b = formatBarode(line)
            editAndSaveBarcode(b, tmp_filename)
            displaySavedImage(tmp_filename)

            key = cv2.waitKey(0) & 0xFF
            if key == 27:
                if os.path.exists(tmp_filename): os.remove(tmp_filename)
                exit(0)

if __name__ == "__main__":
    if len(sys.argv) < 2 or str(sys.argv[1]) == '--help':
        print('Usage: python {} <input_file>'.format(str(sys.argv[0])))
        print('  where <input_file> contains a list of barcode values')
        print('  to convert to images.')
        print('Format: one value per line')
        exit(0)

    path = str(sys.argv[1])
    if not os.path.exists(path):
        print('[ERROR] Input file not found')
        exit(-1)

    displayListOfBarcodes(path)
    cv2.destroyAllWindows()
