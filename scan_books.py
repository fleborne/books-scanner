import time, os, cv2, sys, threading
from datetime import datetime

from selenium import webdriver

import pyzbar.pyzbar as pyzbar
from pyzbar.pyzbar_error import PyZbarError

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import pandas as pd

from dotenv import load_dotenv
load_dotenv()
STORAGE_DIR = os.getenv('STORAGE_DIR', 'data')
SHOPS = [] # ['gibert', 'momox', 'kiwibook']

from books_scanner.commons import (
    searchISBN,
    saveDF,
    checkBarCode
)

from books_scanner.Momox import (
    searchMomox,
    momox_params
)
from books_scanner.Kiwibook import (
    searchKiwibook,
    kiwibook_params
)
from books_scanner.Gibert import (
    searchGibert,
    gibert_params
)

def getBarcodeValue(gray_image, debug=False):
    ''' Extract the value of the barcodes found in a gray scale image

    :param gray_image: A gray scale image of size (height, width, 1) in which
                       barcodes will be searched.
    :return: A list of the detected EAN13 barcodes values (int).
    '''

    decodedObjects = []
    try:
        decodedObjects = pyzbar.decode(gray_image)
        if debug and decodedObjects != []: print(decodedObjects)
    except PyZbarError:
        print('[ERROR] Unable to decode image in getBarcodeValue')

    return [obj.data for obj in decodedObjects if obj.type == 'EAN13']

def initResults(from_file=None):
    # pandas dataframe setup
    if from_file is not None and os.path.exists(from_file):
        df = pd.read_csv(from_file)
    else:
        df = pd.DataFrame({'time': [],
                           'title': [],
                           'isbn': [],
                           'kiwibook': [],
                           'gibert': [],
                           'momox': []})

    return {'df': df,
            'prices': {
                'momox': None,
                'gibert': None,
                'kiwibook': None}}

def displayPrices(frame, barcode_value_display, results):
    # Display settings
    font_family = cv2.FONT_HERSHEY_SIMPLEX
    font_scale = 1
    font_color = (0, 0, 255)
    font_thickness = 1
    line_height = 30

    frame = cv2.putText(frame,
                        ''.join(['EAN13: ', barcode_value_display]),
                        (5, line_height),
                        font_family,
                        font_scale,
                        font_color,
                        font_thickness)

    for idx, shop in enumerate(SHOPS):
        frame = cv2.putText(frame,
                            ''.join([shop, ': ', str(results['prices'][shop])]),
                            (5, (idx+2) * line_height),
                            font_family,
                            font_scale,
                            font_color,
                            font_thickness)

    cv2.imshow('frame', frame)

def commandLineSearch(driver, results, input_):
    try:
        input_ = str(input_)
        while input_ != '':
            try:
                if checkBarCode(input_):
                    singleSearch(driver, input_, results)
                else:
                    print('[ERROR] Barcode is incorrect')
            except ValueError:
                print('[ERROR] Barcode is incorrect')

            input_ = str(input('EAN13:\n'))
    finally:
        driver.close()

def fromFileSearch(driver, results, input_file, csv_file=None):
    df = pd.read_csv(input_file)

    if csv_file is None:
        date_time = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
        csv_file = os.path.join(STORAGE_DIR, 'books_' + date_time + '.csv')

    for idx, row in df.iterrows():
        print('[INFO] {}/{}'.format(idx+1, len(df)))
        results = priceSearch(driver, row['isbn'], results, book_title=row['title'])

    saveDF(results['df'], csv_file)
    driver.close()

def priceSearch(driver, barcode_value, results, book_title='', verbose=False):
    results['prices']['gibert'] = None
    results['prices']['momox'] = None
    results['prices']['kiwibook'] = None

    kiwibook_price = None
    gibert_price = None
    momox_price = None

    if 'momox' in SHOPS:
        try:
            momox_price = searchMomox(driver, barcode_value)
            results['prices']['momox'] = momox_price
            print('[INFO] Momox price:', momox_price)
        except Exception as e:
            print(f'[ERROR] {e}')

    if 'gibert' in SHOPS:
        try:
            gibert_price = searchGibert(driver, barcode_value)
            results['prices']['gibert'] = gibert_price
            print('[INFO] Gibert price:', gibert_price)
        except Exception as e:
            print(f'[ERROR] {e}')

    if 'kiwibook' in SHOPS:
        try:
            kiwibook_price = searchKiwibook(driver, barcode_value)
            results['prices']['kiwibook'] = kiwibook_price
            print('[INFO] Kiwibook price:', kiwibook_price)
        except Exception as e:
            print(f'[ERROR] {e}')

    results['df'] = results['df'].append({'time': time.time(),
                                          'title': book_title,
                                          'isbn': barcode_value,
                                          'kiwibook': kiwibook_price,
                                          'gibert': gibert_price,
                                          'momox': momox_price}, ignore_index=True)
    return results

def singleSearch(driver, barcode_value, results, csv_file='books.csv'):
    print('[INFO] EAN13:', barcode_value)

    book_title = searchISBN(driver, barcode_value)
    print('[INFO] Book title:', book_title)

    results = priceSearch(driver, barcode_value, results, book_title=book_title)
    saveDF(results['df'], csv_file)

def multipleSearch(driver, results):
    lookup_thread = None

    barcode_value = ''
    barcode_value_display = ''
    last_barcode_value = ''
    quit = False

    # input stream setup
    stream_device = os.getenv("WEBCAM_IP_ADDR", default=0)
    cap = cv2.VideoCapture(stream_device)
    cap.set(cv2.CAP_PROP_BUFFERSIZE, 1) # seems to have no effect

    while not quit and cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            continue

        res = getBarcodeValue(cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY))
        if res:
            # we will process only the first barcode detected
            # as we assume that we only scan one book at once.
            barcode_value = res[0].decode("utf-8")

        displayPrices(frame, barcode_value_display, results)

        processing = lookup_thread and lookup_thread.is_alive()
        identical = last_barcode_value == barcode_value
        length_ok = len(barcode_value) == 13

        if not processing and not identical and length_ok:
            print('\a') # ring the bell

            results['prices']['gibert'] = None
            results['prices']['momox'] = None
            results['prices']['kiwibook'] = None

            last_barcode_value = barcode_value
            barcode_value_display = '-'.join([barcode_value[0:1],
                                              barcode_value[1:7],
                                              barcode_value[7:]])

            lookup_thread = threading.Thread(target=singleSearch, args=(driver, barcode_value, results))
            try:
                lookup_thread.start()
            except RuntimeError:
                print('[ERROR] Trying to start the same thread several times')

        key_pressed = cv2.waitKey(1) & 0xFF
        new_frame = False

        if key_pressed == 27 or key_pressed == ord('q'):
            quit = True

    cap.release()
    cv2.destroyAllWindows()

    try:
        if lookup_thread:
            lookup_thread.join()
    except RuntimeError:
        print('[ERROR] Unable to use join() on lookup_thread')

    driver.close()

if __name__ == '__main__':
    if len(sys.argv) == 2 and sys.argv[1] in ['--help', '-h']:
        print('Usage:')
        print('  1) python {} '.format(sys.argv[0]))
        print('  2) python {} <arg>'.format(sys.argv[0]))
        print()
        print('1 - Scan multiple barcodes from the video stream set in WEBCAM_IP_ADDR')
        print('2 - Scan multiple barcodes from values written in the terminal, starting')
        print('    with the one given as an argument (<arg>).')
        print('    If <arg> contains the path to a list of books in a CSV file,')
        print('    looks up the prices of the books contained in the CSV file.')
        exit(0)

    driver = webdriver.Firefox()
    if len(sys.argv) == 2:
        if os.path.exists(sys.argv[1]):
            df = pd.DataFrame({'time': [],
                               'title': [],
                               'isbn': [],
                               'kiwibook': [],
                               'gibert': [],
                               'momox': []})
            results = {'df': df,
                       'prices': {
                           'momox': None,
                           'gibert': None,
                           'kiwibook': None
                      }}

            fromFileSearch(driver,
                           initResults(),
                           sys.argv[1])
        else:
            commandLineSearch(driver,
                              initResults(from_file=os.path.join(STORAGE_DIR, 'books.csv')),
                              sys.argv[1])
    else:
        multipleSearch(driver,
                       initResults(from_file=os.path.join(STORAGE_DIR, 'books.csv')))
