# Books Scanner

* [Install on MacOS](#install-on-macos)
* [Run the app](#run-the-app)
  * [Scan books](#scan-books)
  * [Fix database](#fix-database)
  * [Make buy requests](#make-buy-requests)
  * [Generate barcodes](#generate-barcodes)
* [Use an Android phone to scan](#use-an-android-phone-to-scan)

Books Scanner is a Python app that automatically queries several second hand online shops and returns how much you can get by selling a book on these platforms. It can scan the books' barcode with a laptop's webcam or a smartphone camera, query price information and build a database with the extracted data.

At the moment, it can query the following second hand online shops:

* Momox (https://www.momox.fr/)
* Kiwibook (https://vendre-livre.fr/)
* Gibert (https://www.gibert.com/sao)

Any input video stream readable by OpenCV's VideoCapture (mainly built-in webcam or IP camera) can be used and the database is stored in a CSV file and managed as a Pandas dataframe.

## Install on MacOS

### 1. Install Firefox

Firefox is required for searching the websites.

### 2. Install brew recipes

- zbar: used for barcode decoding
- geckodriver: used for controlling Firefox

```
brew install zbar geckodriver
```

### 3. Install Python dependencies

From this repo, run:
```
python -m venv book-venv
source book-venv/bin/activate
python -m pip install --upgrade pip
python -m pip install -r requirements.txt
```

### 4. Setup the app

The default input video stream is device 0 on the machine running the app. To use a non-default input, set the `WEBCAM_IP_ADDR` environment to the value of the stream identifier. For instance :

```
WEBCAM_IP_ADDR="http://192.168.1.14:6677/videofeed"
```

This can be set in the `.env` file for this project.

## Run the app

### Scan books

In the virtual environment used to install the dependencies, run

```
python scan_books.py
```

After each successful scan, the database will be updated and can be retrieved in file `STORAGE_DIR/books.csv`.

### Fix database

If something went wrong during scanning, it is possible to fix the database by running

```
python fix_db.py <db.csv>
```

This will perform the following:

1. replace 0.0 prices by None (for easier analysis with pandas)
2. look up online for missing titles
3. drop records for which no title can be found
4. save the fixed database to `<db.csv>`

The databases in which the titles are searched are:

* isbndb.com
* isbnsearch.org
* momox.fr

The search is performed in that order, and as soon as a title is found it is returned without querying the next databases.


### Make buy requests

It is possible to check the current price of the items in the library by running

```
python buy_requests.py <db.csv>
```

where `<db.csv>` is a file structured like the output of `scan_books.py`.

### Generate barcodes

Given a list of barcode values (EAN13) in `barcode_list.txt` (one value per line), images corresponding to these barcodes can be successively generated and displayed by the following:

```
python generate_barcodes.py barcode_list.txt
```

Press any key to display the next barcode and `Escape` to stop.

## Use an Android phone to scan

It is possible to use an Android smartphone's camera to scan the barcodes.

### 1. IP Webcam Android app

* Install the **IP Webcam** on the Android device: https://play.google.com/store/apps/details?id=com.pas.webcam

### 2. Scanner settings

Next to the `scan_books.py` file, create a `.env` file and write the following in it:

```
WEBCAM_IP_ADDR="http://<ip addr>:8080/video"
```

where `<ip addr>` is the IP address of the smartphone.

### 3. Run the Python app

```
python scan_books.py
```

The broadcast needs to be started before the Python app.
