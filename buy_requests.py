import sys, os, time
import pandas as pd

from dotenv import load_dotenv
load_dotenv()
STORAGE_DIR = os.getenv('STORAGE_DIR', 'data')

from selenium import webdriver
from selenium.common.exceptions import (
    NoSuchElementException,
    TimeoutException,
    ElementNotInteractableException
)
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from books_scanner.commons import (
    saveDF
)

from books_scanner.Kiwibook import (
    searchBookToBuyOnKiwibook,
    kiwibook_params
)
from books_scanner.Momox import (
    searchBookToBuyOnMomox,
    momox_params
)

def resetDriver(driver, params):
    driver.close()
    driver = webdriver.Firefox()

    if not initBookToBuySearch(driver, params):
        print('[ERROR] Failed to reset driver')
        exit(-1)

    return driver

def initBookToBuySearch(driver, params):
    url = params['url_buy']
    cookie_button_id = params['cookie_button_id']
    search_field_id = params['search_field_id']
    method = params['method']

    driver.get(url)

    if cookie_button_id is not None:
        cookie_consent = WebDriverWait(driver, 4).until(
            EC.element_to_be_clickable((By.ID, cookie_button_id))
        )

        # close cookie consent panel
        cookie_consent.click()
        time.sleep(1)

    try:
        WebDriverWait(driver, 4).until(
            EC.element_to_be_clickable((method, search_field_id))
        )
        return True
    except NoSuchElementException:
        return False

def runSearch(driver, params_list, isbn_list, df_buy):
    def singleSearch(driver, isbn, params):
        search_func = params['search_func']
        price = search_func(driver, isbn, add_to_cart=params['add_to_cart'])
        print('[INFO] [{}] Found item {} with price {} €'.format(params['seller'], isbn, price))
        return price

    for params in params_list:
        if initBookToBuySearch(driver, params):
            for isbn in isbn_list:
                price = None
                try:
                    price = singleSearch(driver, isbn, params)
                except (NoSuchElementException, TimeoutException, ElementNotInteractableException):
                    # reset driver and retry once
                    driver = resetDriver(driver, params)
                    try:
                        price = singleSearch(driver, isbn, params)
                    except (NoSuchElementException, TimeoutException, ElementNotInteractableException):
                        print('[ERROR] [{}] Can\'t find item {}'.format(params['seller'], isbn))

                # except:
                #     print('[ERROR] [{}] An error happened while processing item {}'.format(params['seller'], isbn))
                #     print(sys.exc_info()[0])

                df_buy = df_buy.append({'timestamp': time.time(),
                                        'isbn': isbn,
                                        'price': price,
                                        'seller': params['seller']}, ignore_index=True)
        else:
            print('[ERROR] Failed to initialize web driver')

    return driver, df_buy

if __name__ == '__main__':
    if len(sys.argv) > 1 and sys.argv[1] == '--help':
        print('Usage: python {} input.csv output.csv'.format(sys.argv[0]))
        print('input.csv: must be parsable by pandas and contain a isbn column')
        print('           from which the references to buy will be retrieved.')
        print('output.csv: will be used to store the buy results (book price).')
        exit(0)

    csv_file = sys.argv[1] if len(sys.argv) > 1 else os.path.join('data', 'books.csv')
    buy_db   = sys.argv[2] if len(sys.argv) > 2 else os.path.join('data', 'books_buy.csv')

    print('[WARN] Reading from {} and writing to {}'.format(csv_file, buy_db))
    if input('Continue? [y]/N\n').strip().upper() == 'N':
        exit(0)

    if os.path.exists(csv_file):
        df = pd.read_csv(csv_file)
    else:
        print('[ERROR] No scan database found (file {})'.format(csv_file))
        exit(-1)

    if os.path.exists(buy_db):
        df_buy = pd.read_csv(buy_db)
    else:
        df_buy = pd.DataFrame({'timestamp': [],
                               'isbn'     : [],
                               'price'    : [],
                               'seller'   : []})

    params_list = [kiwibook_params, momox_params]

    driver = webdriver.Firefox()
    driver, df_buy = runSearch(driver,
                               params_list,
                               df['isbn'].astype(str).unique(),
                               df_buy)
    saveDF(df_buy, buy_db)

    if driver:
        driver.close()
        driver = None
